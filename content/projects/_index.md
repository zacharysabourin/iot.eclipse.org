---
title: "Projects"
seo_title: "Projects - Eclipse IoT"
description: "Eclipse IoT is home to the production of quality open source components that our members and ecosystem use to build IoT devices, IoT gateways, Edge nodes and IoT Cloud backends."
keywords: ["Eclipse IoT", "IoT Gateways", "cloud backends", "open source software"]
headline: "Projects"
tagline: "Eclipse IoT open source projects help you build IoT Devices, Gateways (\"Smart Objects\"), Cloud backends, and more. Use the list below to find the project that's right for you."
content_classes: "padding-top-50"
hide_sidebar: true
hide_page_title: true
header_wrapper_class: "header-alternate-bg-img-1"
links: [[href: "/projects/getting-started", text: "Get Started"], [href: "/projects/sandboxes", text: "Sandboxes"]]
---

{{< eclipsefdn_projects
    templateId="tpl-projects-item"
    url="https://projects.eclipse.org/api/projects?working_group=internet-things-iot"
    classes="margin-top-30"
    display_categories="false"
    categories=""
>}}

