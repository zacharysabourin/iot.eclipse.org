---
title: "Virtual IoT & Edge Days 2022"
description: ""
headline: "Virtual IoT and <br> Edge Days 2022"
tagline: "Showcasing Innovation in IoT and Edge Computing <br> <span>Virtual Conference | May 24 - 25, 2022<span>"
date: 2022-02-05T16:09:45-04:00
hide_page_title: true
hide_sidebar: true
hide_breadcrumb: true
header_wrapper_class: "iot-day-2022"
links: [[href: "https://iot-edge-days-2022.eventbrite.com", text: "Register Now"]]
container: container-fluid
layout: single
---

{{< grid/section-container id="registration" class="padding-top-10 padding-bottom-60 text-center" >}}
  {{< events/registration title="About the Event" event="eclipse-virtual-iot-edge-2022">}}
Virtual IoT and Edge Days 2022 is an online event for IoT and Edge developers, with an emphasis on Eclipse-driven projects and technologies. The aim of the event is to gather our contributors and committers to share information that encourages collaboration among the various projects.

The event is open to any developer, product manager or architect interested in IoT and Edge topics. It is hosted jointly by the Eclipse IoT, Edge Native, and Sparkplug working groups at the Eclipse Foundation.
  {{</ events/registration >}}
  <a href="https://iot-edge-days-2022.eventbrite.com" class="btn btn-primary margin-top-10" >Register Now</a>
{{</ grid/section-container >}}


{{< grid/section-container id="speakers" class="row text-center padding-top-20 padding-bottom-20">}}
{{< events/user_display event="eclipse-virtual-iot-edge-2022" title="Speakers" source="speakers" imageRoot="/assets/images/speakers/" subpage="speakers" displayLearnMore="false" />}}
{{</ grid/section-container >}}

{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-lighter-bg agenda-2022">}}
{{< grid/div class="padding-bottom-20" isMarkdown="false" >}}
{{< events/agenda event="eclipse-virtual-iot-edge-2022" >}}
{{</ grid/div >}}
{{</ grid/section-container >}}

 <!-- NOTE: hosted by and sponsors sections should always be the last ones on the page -->
{{< grid/section-container id="hosted-by" class="padding-top-20 padding-bottom-20" >}}
  <h2 class="text-center">Hosted by</h2>
  <p class="text-center">IoT & Edge Days 2022 is hosted by the Eclipse IoT, Edge Native, and Sparkplug working groups at the Eclipse Foundation.</p>
  {{< events/sponsors headerClass="hide" source="hostedBy" event="eclipse-virtual-iot-edge-2022" useMax="false" displayBecomeSponsor="false" >}}

{{</ grid/section-container >}}

{{< grid/section-container id="sponsors" class="white-row padding-top-20 padding-bottom-20 text-center" >}}
  {{< events/sponsors event="eclipse-virtual-iot-edge-2022" title="Thanks to Our Participating Member Companies" useMax="false" displayBecomeSponsor="false" >}}
{{</ grid/section-container >}}

{{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}
