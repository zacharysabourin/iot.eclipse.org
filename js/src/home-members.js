/*!
 * Copyright (c) 2020 Eclipse Foundation, Inc.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * Contributors:
 *   Christopher Guindon <chris.guindon@eclipse-foundation.org>
 *   Zhou Fang <zhou.fang@eclipse-foundation.org>
 * SPDX-License-Identifier: EPL-2.0
 */

const pickThreeRandomMembers = (() => {
  const memberListElement = document.querySelector('.eclipsefdn-members-list.home-member-list');

  if (!memberListElement) {
    return
  }

  const callback = () => {
    observer.disconnect();
    const allMemberItems = memberListElement.querySelectorAll('.members-item');
    if (allMemberItems.length === 0) {
      return
    }
    memberListElement.innerHTML = '';
    // The member list is in random order so just pick first 3 items
    for (let i = 0; i < 3; i++) {
      memberListElement.appendChild(allMemberItems[i]);
    }
    document.getElementById('iot-member-list').classList.add('hide');
    memberListElement.classList.remove('hide');
  };

  const observer = new MutationObserver(callback);
  observer.observe(memberListElement, { childList: true });
})();
